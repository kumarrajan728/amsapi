﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AttendanceManagementSystem.API.Models;
using AttendanceManagementSystem.API.DataAccessLayer;
using AttendanceManagementSystem.API.ViewModels;

namespace AttendanceManagementSystem.API.Controllers
{
    [RoutePrefix("api/emptype")]
    public class EmpTypeController : ApiController
    {
        [HttpGet]
        [Route("getall")]
        public IEnumerable<EmpTypeVM> GetEmployeeTypes()
        {
            using (AMSContext ctx = new AMSContext())
            {
                List<EmpType> emptypes = ctx.emptypes.ToList();

                IEnumerable<EmpTypeVM> etVM = emptypes.Select(et => new EmpTypeVM() { Id = et.Id, Type = et.Type });

                return etVM;
            }
        }

        [HttpGet]
        [Route("get")]
        public EmpType GetEmpType(int id)
        {
            using (var ctx = new AMSContext())
            {
                EmpType empType = ctx.emptypes.Find(id);
                return empType;
            }
        }

        [HttpPost]
        [Route("create")]
        public void CreateEmpType([FromBody] EmpTypeVM type)
        {
            using (var ctx = new AMSContext())
            {
                EmpType empType = new EmpType() { Type= type.Type };
                ctx.emptypes.Add(empType);
                ctx.SaveChanges();
            }    
        }

        [HttpPut]
        [Route("update")]
        public void UpdateEmpType(int id, [FromBody] EmpTypeVM type)
        {
            using(var ctx = new AMSContext())
            {
                EmpType empType = ctx.emptypes.Find(id);
                empType.Type = type.Type;

                ctx.SaveChanges();
                
            }
        }

        [HttpDelete] 
        [Route("delete")]
        public void DeleteEmpTypes(int id)
        {
            using(var ctx = new AMSContext())
            {
                EmpType empType = ctx.emptypes.Find(id);
                ctx.emptypes.Remove(empType);

                ctx.SaveChanges();

            }
        }

    }
}
