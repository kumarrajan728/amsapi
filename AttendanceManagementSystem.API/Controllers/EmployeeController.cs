﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AttendanceManagementSystem.API.Models;
using AttendanceManagementSystem.API.DataAccessLayer;
using AttendanceManagementSystem.API.ViewModels;
using System;

namespace AttendanceManagementSystem.API.Controllers
{
    [RoutePrefix("api/employee")]
    public class EmployeeController : ApiController
    {
       [HttpGet]
       [Route("getall")]
       public IEnumerable<EmployeeVM> GetAllEmployees() 
        {
            using(var ctx = new AMSContext())
            {
                List<Employee> employees = ctx.employees.Include("Type").ToList();
                IEnumerable<EmployeeVM> empVM = employees.Select(em => new EmployeeVM() { Id = em.Id, Name = em.Name, Type = em.Type.Type});
                return empVM;
            }
        }

        [HttpGet]
        [Route("getallwithtype")]
        public List<EmployeeVM> GetAllEmplyeesWithTypes()
        {
            using(var ctx = new AMSContext())
            {
                List<Employee> employees = ctx.employees.Include("Type").ToList();

                List<EmployeeVM> vMs = new List<EmployeeVM>();
                foreach (Employee emp in employees)
                {
                   
                    EmployeeVM vm = new EmployeeVM();
                    vm.Id = emp.Id;
                    vm.Name = emp.Name;
                    vm.Type = emp.Type.Type;

                    vMs.Add(vm);
                }

                return vMs;
            }
        }

        [HttpGet]
        [Route("get")]
        public Employee GetEmployee(int id)
        {
            using (AMSContext ctx = new AMSContext())
            {
                Employee employee = ctx.employees.Find(id);
                return employee;
                
            }
        }

        [HttpPost]
        [Route("create")]
        public void CreateEmployee([FromBody] Employee emp)
        {
            using(var ctx = new AMSContext())
            {
                Employee employee = new Employee() { Name = emp.Name, EmpTypeId = emp.EmpTypeId };
                ctx.employees.Add(employee);

                ctx.SaveChanges();
            }
        }

        [HttpPut]
        [Route("update")]
        public void UpdateEmployee(int id, string name, int emptypeid)
        {
            using (AMSContext context = new AMSContext())
            {
                Employee employee = context.employees.Find(id);
                employee.Name = name;
                employee.EmpTypeId = emptypeid;

                context.SaveChanges();
            }
        }
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteEmployee(int id) 
        { 
            using(AMSContext ctx = new AMSContext())
            {
                Employee employee = ctx.employees.Find(id);
                ctx.employees.Remove(employee);

                try
                {
                    ctx.SaveChanges();
                    
                    return Ok();
                }
                catch (Exception)
                {
                    throw new InvalidOperationException("Couldn't delete employee because it has some attendences assigned. " +
                        "Delete attendences and try again.");
                }
            }  
            
        }
    }
}
