﻿using AttendanceManagementSystem.API.DataAccessLayer;
using AttendanceManagementSystem.API.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System;
using AttendanceManagementSystem.API.ViewModels;
 
namespace AttendanceManagementSystem.API.Controllers
{
    [RoutePrefix("api/attendence")]
    public class AttendanceController : ApiController
    {
        [HttpGet]
        [Route("getall")]
        public IEnumerable<AttendenceVM> GetAllAttendences()
        {
            using (var ctx = new AMSContext())
            {
                List<Attendence> attendences = ctx.attendences.ToList();
                IEnumerable<AttendenceVM> attendVM = attendences.Select(att => new AttendenceVM() { Id = att.Id, Date = att.Date, EmpId = att.EmpId, IsPresent = att.Ispresent });
                return attendVM;
            }
        }

        [HttpGet]
        [Route("get")]
        public Attendence GetAttendence(int id)
        {
            using (AMSContext ctx = new AMSContext())
            {
                Attendence attendence = ctx.attendences.Find(id);
                return attendence;
            }
        }
        
        [HttpPost]
        [Route("create")]
        public void PostAttendence(int empid, DateTime now, bool ispresent)
        {
            using (AMSContext ctx = new AMSContext())
            {
                Attendence attendence = new Attendence();
                attendence.EmpId = empid;
                attendence.Ispresent = ispresent;
                attendence.Date = now;

                ctx.attendences.Add(attendence);

                ctx.SaveChanges();

            }
        }

        [HttpPut]
        [Route("update")]
        public void PutAttendence(int id, int empid, DateTime now, bool ispresent)
        {
            using (var ctx = new AMSContext())
            {
                Attendence attendence = ctx.attendences.Find(id);
                attendence.EmpId = empid;
                attendence.Date = now;
                attendence.Ispresent = ispresent;

                ctx.SaveChanges();
            }

        }

        [HttpDelete]
        [Route("delete")]
        public void DeleteAttendence(int id)
        {
            using (AMSContext ctx = new AMSContext())
            {
                Attendence attendence = ctx.attendences.Find(id);
                ctx.attendences.Remove(attendence);

                ctx.SaveChanges();

            }
        }
    }
}
