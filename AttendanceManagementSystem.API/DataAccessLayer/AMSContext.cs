﻿using System.Data.Entity;
using AttendanceManagementSystem.API.Models;

namespace AttendanceManagementSystem.API.DataAccessLayer
{
    public class AMSContext: DbContext
    {
        public AMSContext(): base("name=dbconn") 
        {
            Database.SetInitializer<AMSContext>(null);
        }

        public DbSet<EmpType> emptypes { get; set; }
        public DbSet<Employee> employees { get; set; }
        public DbSet<Attendence> attendences { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Fluent API
            modelBuilder.Entity<EmpType>().ToTable("EmpType");
            modelBuilder.Entity<Employee>().ToTable("Employee");
            modelBuilder.Entity<Attendence>().ToTable("Attendence");

            modelBuilder.Entity<EmpType>()
                .Property(t => t.Type)
                .HasColumnName("Type");

            modelBuilder.Entity<Employee>()
                .HasRequired<EmpType>(e => e.Type)
                .WithMany(t => t.Employees)
                .HasForeignKey<int>(e => e.EmpTypeId);
            modelBuilder.Entity<Attendence>()
                .HasRequired<Employee>(a => a.Employee)
                .WithMany(e => e.Attendences)
                .HasForeignKey<int>(a => a.EmpId);
        }
    }
}