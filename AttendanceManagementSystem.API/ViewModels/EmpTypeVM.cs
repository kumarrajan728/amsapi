﻿namespace AttendanceManagementSystem.API.ViewModels
{
    public class EmpTypeVM
    {
        public int Id { get; set; }

        public string Type { get; set; }
    }
}