﻿namespace AttendanceManagementSystem.API.ViewModels
{
    public class EmployeeVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}