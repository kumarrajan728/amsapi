﻿using System;

namespace AttendanceManagementSystem.API.ViewModels
{
    public class AttendenceVM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public bool IsPresent { get; set; }

        public int EmpId { get; set; }
    }
}