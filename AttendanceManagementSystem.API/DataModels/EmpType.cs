﻿using System.Collections.Generic;

namespace AttendanceManagementSystem.API.Models
{
    public class EmpType
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public List<Employee> Employees { get; set; }

        public EmpType()
        {
            Employees = new List<Employee>();
        }
    }
}