﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttendanceManagementSystem.API.Models
{
    public class Attendence
    {
        public int Id { get; set; }        
        public DateTime Date { get; set; }
        public bool Ispresent { get; set; }
        public int EmpId { get; set; }
        public Employee Employee { get; set; }
    }
} 