﻿using System.Collections.Generic;

namespace AttendanceManagementSystem.API.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EmpTypeId { get; set; }
        public EmpType Type { get; set; }
        public List<Attendence> Attendences { get; set; }
        public Employee()
        {
            Attendences = new List<Attendence>();
        }
    }
}